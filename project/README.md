# Advanced Information Systems Interoperability 

### JAVAFX CLIENT project

If you would like more information about the Java FX client, follow this link :
[README.md](./first-assignement/javafx-client/README.md)  

### CLIENT project

If you would like more information about the Angular Client, follow this link :
[README.md](./second-assignement/client/README.md)  

### REST API project

If you would like more information about the API of the first assignement, follow this link :
[README.md](./first-assignement/rest-api/README.md)  

If you would like more information about the API of the second assignement, follow this link :
[README.md](./second-assignement/rest-api/README.md)  

### MySQL Database information

If you would like more information about the MySQL database of the first assignement, follow this link :
[README.md](./first-assignement/sql/README.md)  

If you would like more information about the MySQL database of the second assignement, follow this link :
[README.md](./second-assignement/sql/README.md)  

### ActiveMQ integration

To access the ActiveMQ server, follow this [url](http://localhost:8161/admin/queues.jsp) and enter the credentials
