package javafxclient.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicStatusLine;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class HttpRequestService {

    private final String restApiHost = System.getenv("API_HOST_URL");

    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    public HttpResponseBody get(String path, Map<String, String> headers, Map<String, String> params) throws IOException {
        HttpGet request = new HttpGet(this.restApiHost + HttpRequestService.createQueries(path, params));

        HttpRequestService.createHeaders(request, headers);
        try {
            CloseableHttpResponse response = httpClient.execute(request);
            return new HttpResponseBody(response.getEntity(), response.getStatusLine(), response.getAllHeaders());
        } catch (Exception exception) {
            ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 0, 0);
            return new HttpResponseBody(new BasicStatusLine(protocolVersion, 500, "API Problem"));
        }
    }

    public HttpResponseBody post(String path, Object object, Map<String, String> headers, Map<String, String> params) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        HttpPost request = new HttpPost(this.restApiHost + HttpRequestService.createQueries(path, params));

        request.setEntity(new StringEntity(mapper.writeValueAsString(object)));
        HttpRequestService.createHeaders(request, headers);
        try {
            CloseableHttpResponse response = httpClient.execute(request);
            return new HttpResponseBody(response.getEntity(), response.getStatusLine(), response.getAllHeaders());
        } catch (Exception exception) {
            ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 0, 0);
            return new HttpResponseBody(new BasicStatusLine(protocolVersion, 500, "API Problem"));
        }
    }

    private static void createHeaders(HttpRequestBase httpRequestBase, Map<String, String> headers) {
        httpRequestBase.setHeader("Accept", "application/json");
        httpRequestBase.setHeader("Content-type", "application/json");

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            httpRequestBase.addHeader(entry.getKey(), entry.getValue());
        }
    }

    private static String createQueries(String url, Map<?, ?> queries) {
        StringBuilder sb = new StringBuilder();

        sb.append(url);
        for (Map.Entry<?, ?> entry : queries.entrySet()) {
            if (sb.length() == 1) {
                sb.append("?");
            }
            if (sb.length() > 1) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s", HttpRequestService.urlEncodeUTF8(entry.getKey().toString()),
                    HttpRequestService.urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }

    private static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
