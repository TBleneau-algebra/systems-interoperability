package javafxclient.services;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class HttpResponseBody {

    private HashMap<String, String> headers;

    private int code;

    private String message;

    private JSONObject data;

    public HttpResponseBody() {
        this.data = null;
        this.headers = new HashMap<>();
    }

    public HttpResponseBody(StatusLine statusLine) {
        this.message = statusLine.getReasonPhrase();
        this.code = statusLine.getStatusCode();
        this.data = null;
        this.headers = new HashMap<>();
    }

    public HttpResponseBody(HttpEntity httpEntity, StatusLine statusLine, Header[] httpHeaders) {
        this.message = statusLine.getReasonPhrase();
        this.code = statusLine.getStatusCode();
        this.createHeaders(httpHeaders);
        this.createBody(httpEntity);
    }

    private void createBody(HttpEntity httpEntity) {
        try {
            String line;
            StringBuilder result = new StringBuilder();
            BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));

            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            if (!result.toString().equals("")) {
                this.data = new JSONObject(result.toString());
            } else {
                this.data = null;
            }
        } catch (IOException e) {
            this.data = null;
        }
    }

    private void createHeaders(Header[] httpHeaders) {
        this.headers = new HashMap<>();

        for (Header header : httpHeaders) {
            this.headers.put(header.getName(), header.getValue());
        }
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public HashMap<String, String> getHeaders() {
        return this.headers;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

}
