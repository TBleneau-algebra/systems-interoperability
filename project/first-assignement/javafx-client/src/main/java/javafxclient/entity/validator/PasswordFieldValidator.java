package javafxclient.entity.validator;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class PasswordFieldValidator {

    private static final Map<Integer, String> messages = ImmutableMap.<Integer, String>builder()
            .put(0, "Password is required")
            .put(1, "Password length must be between 8 and 64")
            .put(2, "Password should contain 1 special character")
            .put(3, "Password should contain 1 uppercase")
            .put(4, "Password should contain 1 lowercase")
            .put(5, "Password should contain 1 digit")
            .put(6, "Password should not contain white spaces")
            .build();

    private static boolean lengthValidation(String password) {
        return password.length() >= 8 && password.length() <= 64;
    }

    private static boolean specialCharsValidation(String password) {
        return password.matches("(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)");
    }

    private static boolean uppercaseValidation(String password) {
        return password.matches("(.*[A-Z].*)");
    }

    private static boolean lowercaseValidation(String password) {
        return password.matches("(.*[a-z].*)");
    }

    private static boolean digitValidation(String password) {
        return password.matches("(.*[0-9].*)");
    }

    private static boolean whiteSpaceValidation(String password) {
        return password.matches("(.*[,\t,\n,\u000b,\f,\n,' '].*$)");
    }

    public static boolean requiredValidation(String password) {
        return password == null || password.isEmpty();
    }

    public static String isValid(String password) {
        if (PasswordFieldValidator.requiredValidation(password)) {
            return PasswordFieldValidator.messages.get(0);
        }
        if (!PasswordFieldValidator.lengthValidation(password)) {
            return PasswordFieldValidator.messages.get(1);
        }
        if (!PasswordFieldValidator.specialCharsValidation(password)) {
            return PasswordFieldValidator.messages.get(2);
        }
        if (!PasswordFieldValidator.uppercaseValidation(password)) {
            return PasswordFieldValidator.messages.get(3);
        }
        if (!PasswordFieldValidator.lowercaseValidation(password)) {
            return PasswordFieldValidator.messages.get(4);
        }
        if (!PasswordFieldValidator.digitValidation(password)) {
            return PasswordFieldValidator.messages.get(5);
        }
        if (PasswordFieldValidator.whiteSpaceValidation(password)) {
            return PasswordFieldValidator.messages.get(6);
        }
        return null;
    }
}
