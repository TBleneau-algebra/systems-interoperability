package javafxclient.entity.validator;

public class TextFieldValidator {

    public static boolean requiredValidation(String field) {
        return field == null || field.isEmpty();
    }

    public static String isValid(String password) {
        if (TextFieldValidator.requiredValidation(password)) {
            return "This field is required";
        }
        return null;
    }

}
