package javafxclient.entity.validator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class EmailFieldValidator {

    public static boolean formatValidation(String email) {
        boolean result = true;

        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static boolean requiredValidation(String email) {
        return email == null || email.isEmpty();
    }

    public static String isValid(String email) {
        if (EmailFieldValidator.requiredValidation(email)) {
            return "Your email is required";
        }
        if (!EmailFieldValidator.formatValidation(email)) {
            return "Wrong email address format";
        }
        return null;
    }
}
