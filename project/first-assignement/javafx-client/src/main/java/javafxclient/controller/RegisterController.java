package javafxclient.controller;

import javafxclient.entity.model.RegisterClass;
import javafxclient.entity.validator.EmailFieldValidator;
import javafxclient.entity.validator.PasswordFieldValidator;
import javafxclient.entity.validator.TextFieldValidator;
import javafxclient.services.HttpRequestService;
import javafxclient.services.HttpResponseBody;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {

    private HttpRequestService httpRequestService;

    @FXML
    private Button registerButton;

    @FXML
    private Button backButton;

    @FXML
    private TextField name;

    @FXML
    private TextField lastname;

    @FXML
    private TextField username;

    @FXML
    private TextField email;

    @FXML
    private PasswordField password;

    @FXML
    private Text errorNames;

    @FXML
    private Text errorUsername;

    @FXML
    private Text errorEmail;

    @FXML
    private Text errorPassword;

    @FXML
    private Text successText;

    public RegisterController() {
        this.httpRequestService = new HttpRequestService();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.disabled();

        this.initializeNames();
        this.initializeUsername();
        this.initializeEmail();
        this.initializePassword();
    }

    @FXML
    private void navigate() throws IOException {
        Parent sample = FXMLLoader.load(getClass().getResource("/scenes/login.fxml"));
        Scene scene = this.backButton.getScene();

        scene.setRoot(sample);
    }

    @FXML
    private void register() throws IOException {
        RegisterClass registerClass = new RegisterClass(this.name.getText(), this.lastname.getText(), this.email.getText(),
                this.username.getText(), this.password.getText());

        if (!this.disabled()) {
            HttpResponseBody httpResponseBody = this.httpRequestService.post("/user/register", registerClass,
                    Collections.<String, String>emptyMap(), Collections.<String, String>emptyMap());

            switch (httpResponseBody.getCode()) {
                case 201:
                    this.successEvent();
                    break;
                case 409:
                    this.errorEmail.setText("Your email address is probably already used");
                    this.errorUsername.setText("Your username is probably already used");
                    break;
                case 500:
                    this.errorEvent();
                    break;
            }
        }
    }

    private void initializeNames() {
        this.name.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue && !newValue && TextFieldValidator.requiredValidation(name.getText())) {
                errorNames.setText("Your name and last name are required");
            } else {
                errorNames.setText(null);
            }
        });

        this.name.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
        });

        this.lastname.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue && !newValue && TextFieldValidator.requiredValidation(lastname.getText())) {
                errorNames.setText("Your name and last name are required");
            } else {
                errorNames.setText(null);
            }
        });

        this.lastname.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
        });
    }

    private void initializeUsername() {
        this.username.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue && !newValue && TextFieldValidator.requiredValidation(username.getText())) {
                errorUsername.setText("Your username is required");
            } else {
                errorUsername.setText(null);
            }
        });

        this.username.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
        });
    }

    private void initializeEmail() {
        this.email.focusedProperty().addListener((observable, oldValue, newValue) -> {
            String validation = EmailFieldValidator.isValid(email.getText());

            if (oldValue && !newValue && validation != null) {
                errorEmail.setText(validation);
            } else {
                errorEmail.setText(null);
            }
        });

        this.email.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
        });
    }

    private void initializePassword() {
        this.password.focusedProperty().addListener((observable, oldValue, newValue) -> {
            String validation = PasswordFieldValidator.isValid(password.getText());

            if (oldValue && !newValue && validation != null) {
                errorPassword.setText(validation);
            } else {
                errorPassword.setText(null);
            }
        });

        this.password.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
        });
    }

    private boolean disabled() {
        if (TextFieldValidator.requiredValidation(name.getText()) || TextFieldValidator.requiredValidation(lastname.getText()) ||
                TextFieldValidator.requiredValidation(username.getText()) || EmailFieldValidator.isValid(email.getText()) != null ||
                PasswordFieldValidator.isValid(password.getText()) != null) {
            this.registerButton.setDisable(true);
            return true;
        } else {
            this.registerButton.setDisable(false);
        }
        return false;
    }

    private void successEvent() {
        this.name.setVisible(false);
        this.lastname.setVisible(false);
        this.email.setVisible(false);
        this.username.setVisible(false);
        this.password.setVisible(false);
        this.registerButton.setVisible(false);

        this.errorUsername.setText(null);
        this.errorEmail.setText(null);
        this.errorPassword.setText(null);
        this.errorNames.setText(null);

        this.successText.setText("Registration Successful!");
    }

    private void errorEvent() {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setContentText("API problem. An error has occurred");
        alert.showAndWait();
    }
}
