package javafxclient.controller;

import javafxclient.entity.model.LoginClass;
import javafxclient.entity.validator.PasswordFieldValidator;
import javafxclient.entity.validator.TextFieldValidator;
import javafxclient.services.HttpRequestService;
import javafxclient.services.HttpResponseBody;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private HttpRequestService httpRequestService;

    @FXML
    private Button registerButton;

    @FXML
    private Button loginButton;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Text errorUsername;

    @FXML
    private Text errorPassword;

    public LoginController() {
        this.httpRequestService = new HttpRequestService();
    }

    @FXML
    private void login() throws IOException {
        if (!this.disabled()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            HttpResponseBody httpResponseBody = this.httpRequestService.post("/user/login",
                    new LoginClass(username.getText(), password.getText()), Collections.<String, String>emptyMap(), Collections.<String, String>emptyMap());

            switch (httpResponseBody.getCode()) {
                case 200:
                    this.navigateToHome();
                    break;
                case 404:
                    this.errorPassword.setText("This username or password doesn't exist");
                    break;
                case 500:
                    alert.setContentText("API problem. An error has occurred");
                    alert.showAndWait();
                    break;
            }
        }
    }

    @FXML
    private void navigateToRegister() throws IOException {
        Parent sample = FXMLLoader.load(getClass().getResource("/scenes/register.fxml"));
        Scene scene = this.registerButton.getScene();

        scene.setRoot(sample);
    }

    @Override
    public void initialize(final URL location, ResourceBundle resources) {
        this.disabled();
        this.initializeUsername();
        this.initializePassword();
    }

    private void initializeUsername() {
        this.username.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue && !newValue && TextFieldValidator.requiredValidation(username.getText())) {
                errorUsername.setText("Your username is required");
            } else {
                errorUsername.setText(null);
            }
        });

        this.username.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
            this.errorPassword.setText(null);
        });
    }

    private void initializePassword() {
        this.password.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue && !newValue && PasswordFieldValidator.requiredValidation(password.getText())) {
                errorPassword.setText("Your password is required");
            } else {
                errorPassword.setText(null);
            }
        });

        this.password.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
            this.errorPassword.setText(null);
        });
    }

    private boolean disabled() {
        if (TextFieldValidator.requiredValidation(username.getText()) || PasswordFieldValidator.requiredValidation(password.getText())) {
            this.loginButton.setDisable(true);
            return true;
        } else {
            this.loginButton.setDisable(false);
        }
        return false;
    }

    private void navigateToHome() throws IOException {
        Parent sample = FXMLLoader.load(getClass().getResource("/scenes/home.fxml"));
        Scene scene = this.loginButton.getScene();

        scene.setRoot(sample);
    }
}
