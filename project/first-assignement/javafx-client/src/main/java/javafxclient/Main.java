package javafxclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        String sampleFolder = "/scenes/";
        Parent pane = FXMLLoader.load(getClass().getResource(sampleFolder + "login.fxml"));

        primaryStage.setScene(new Scene(pane));
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
