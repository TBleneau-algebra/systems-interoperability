# JAVA FX CLIENT

# Install Java SDK 8

Requirement : The first step to build or run this project is to install [Java JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html). 

## Environment requirement

To launch the Java FX client, you need to provide the API_HOST_URL variable to your development environment 

## Build the API's JAR file

JAR files are packaged with the ZIP file format, so you can use them for tasks such as lossless data compression, archiving, decompression, and archive unpacking.

```sh
      ./mvnw clean package
```

## Git commit format

The first line of the commit message must contains the tags depending on the commit's feature.
The tags hierarchy must be ascendant, that means from the most general to the most precise.

Example 1 :

```sh 
      git commit -m "[CLIENT][JAVAFX][CONTROLLER][LOGIN] - Creation of the JavaFX login sample controller"
```

Example 2 : 

```sh
      git commit -m "[CLIENT][JAVAFX][SAMPLE][HOME] - Creation of the JavaFX home sample (without controller for now)"
```

## Code comment format

To comment the code in Java, we use the official Javadoc.
Javadoc uses a tag system that you can find at these addresses : 

- [Javadoc Tags](https://www.tutorialspoint.com/java/java_documentation.htm)

For example :

```sh 
      /**
       * <h1>Hello, World!</h1>
       * The HelloWorld program implements an application that
       * simply displays "Hello World!" to the standard output.
       * <p>
       * Giving proper comments in your program makes it more
       * user friendly and it is assumed as a high quality code.
       * 
       *
       * @author  xxxxxx yyyyyy
       * @version 1.0
       * @since   2014-03-31 
       */
      public class HelloWorld {
      }
```
```sh 
      /**
       * This method is used to add two integers. This is
       * a the simplest form of a class method, just to
       * show the usage of various javadoc Tags.
       *
       * @param numA This is the first paramter to addNum method
       * @param numB  This is the second parameter to addNum method
       * @return int This returns sum of numA and numB.
       */
      public int addNum(int numA, int numB) {
         return numA + numB;
      }
```

All of your code must be commented.
If the code is not commented correctly, the merge request and your task will not be validated.

## Merge request on the dev branch

If you want to merge a branch into dev branch, first make sure you push all your commits on the branch itself. 
Next, run the following command : 

```sh
      git push origin dev
```

This command will prohibit you to push and display a link to make a merge request on your GitLab account.
Follow this link and complete the merge request. This will notify the maintainers of the repository to evaluate your merge request.
He will then accept or refuse your merge request.
