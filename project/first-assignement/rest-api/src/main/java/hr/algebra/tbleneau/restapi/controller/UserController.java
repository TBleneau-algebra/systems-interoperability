package hr.algebra.tbleneau.restapi.controller;

import hr.algebra.tbleneau.restapi.constant.RoleConstant;
import hr.algebra.tbleneau.restapi.constant.TokenConstant;
import hr.algebra.tbleneau.restapi.entity.UserEntity;
import hr.algebra.tbleneau.restapi.repository.RoleRepository;
import hr.algebra.tbleneau.restapi.repository.UserRepository;
import hr.algebra.tbleneau.restapi.security.SecurityCookieService;
import hr.algebra.tbleneau.restapi.security.SecurityJWTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserRepository userRepository, RoleRepository roleRepository,
                          PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = GET, path = "", produces = "application/json")
    public ResponseEntity<List<UserEntity>> get() {
        return ResponseEntity.status(HttpStatus.OK).body(this.userRepository.findAll());
    }

    @RequestMapping(method = GET, path = "/me", produces = "application/json")
    public ResponseEntity<UserEntity> get(HttpServletRequest httpServletRequest) {
        String username = SecurityJWTokenService.getSubject(httpServletRequest);

        if (!this.userRepository.existsByUsername(username)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        return ResponseEntity.status(HttpStatus.OK).body(this.userRepository.findByUsername(username));
    }

    @RequestMapping(method = POST, path = "/register", produces = "application/json")
    public ResponseEntity<UserEntity> post(@Valid @RequestBody UserEntity userEntity, Errors errors) {
        if (errors.hasErrors() || !this.roleRepository.existsByName(RoleConstant.ROLE_USER.name())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        if (this.userRepository.existsByEmail(userEntity.getEmail()) ||
                this.userRepository.existsByUsername(userEntity.getUsername())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }

        userEntity.getRoles().add(this.roleRepository.findByName(RoleConstant.ROLE_USER.name()));
        userEntity.setPassword(this.passwordEncoder.encode(userEntity.getPassword()));

        this.userRepository.save(userEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(userEntity);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = PUT, path = "/update", produces = "application/json")
    public ResponseEntity<UserEntity> put(@RequestBody UserEntity userEntity, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        UserEntity userToUpdate = this.userRepository.findByUsername(userEntity.getUsername());

        if (this.userRepository.existsByUsername(userEntity.getUsername()) &&
                !userEntity.getUsername().equals(userToUpdate.getUsername())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }

        if (this.userRepository.existsByEmail(userEntity.getEmail()) &&
                !userEntity.getEmail().equals(userToUpdate.getEmail())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }

        userToUpdate.setEmail(userEntity.getEmail());
        userToUpdate.setUsername(userEntity.getUsername());
        userToUpdate.setLastname(userEntity.getLastname());
        userToUpdate.setName(userEntity.getName());

        this.userRepository.save(userToUpdate);
        return ResponseEntity.status(HttpStatus.OK).body(userToUpdate);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = DELETE, produces = "application/json")
    public ResponseEntity<Object> delete(@PathVariable(value = "id") Long id) {
        if (!this.userRepository.existsById(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (this.userRepository.findById(id).get().hasRole("ROLE_ADMIN")) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }

        this.userRepository.delete(this.userRepository.findById(id).get());
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        if (httpServletRequest.getSession(false) != null)
            httpServletRequest.getSession(false).invalidate();

        SecurityCookieService.clear(httpServletResponse, TokenConstant.TOKEN_NAME);
        SecurityCookieService.clear(httpServletResponse, TokenConstant.TOKEN_AUTHENTICATED_NAME);
        httpServletResponse.setStatus(200);
    }
}
