package hr.algebra.tbleneau.restapi.repository;


import hr.algebra.tbleneau.restapi.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByUsername(String username);

    UserEntity findByEmail(String username);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);
}
