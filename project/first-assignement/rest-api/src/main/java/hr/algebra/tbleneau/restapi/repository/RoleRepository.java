package hr.algebra.tbleneau.restapi.repository;


import hr.algebra.tbleneau.restapi.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByName(String name);

    boolean existsByName(String name);
}
