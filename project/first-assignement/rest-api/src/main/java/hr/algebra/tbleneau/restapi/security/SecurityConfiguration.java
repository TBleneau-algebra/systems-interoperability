package hr.algebra.tbleneau.restapi.security;

import hr.algebra.tbleneau.restapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.List;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserRepository userRepository;
    private final UserDetailsService userDetailsService;

    @Value("${restapi.login.url}")
    private String loginUrl;

    @Value("${restapi.url.origin.allowed}")
    private List<String> allowedOrigins;


    public SecurityConfiguration(SecurityUserDetailsService securityUserDetailsService, UserRepository userRepository) {
        this.userDetailsService = securityUserDetailsService;
        this.userRepository = userRepository;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().configurationSource(this.corsConfigurationSource()).and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/user/register").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/user/login").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .addFilter(authenticationFilter())
                .addFilter(new SecurityJWTokenAuthorizationFilter(authenticationManager()))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    private SecurityJWTokenAuthenticationFilter authenticationFilter() throws Exception {
        SecurityJWTokenAuthenticationFilter filter = new SecurityJWTokenAuthenticationFilter(userRepository, authenticationManager());

        filter.setFilterProcessesUrl(loginUrl);
        return filter;
    }


    private CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration configuration = new CorsConfiguration();

        configuration.applyPermitDefaultValues().setAllowedOrigins(this.allowedOrigins);
        configuration.applyPermitDefaultValues().setAllowCredentials(true);
        //configuration.applyPermitDefaultValues().setExposedHeaders(Arrays.asList());
        configuration.applyPermitDefaultValues().setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }
}
