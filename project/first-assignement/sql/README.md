# SQL Database

### Initialize the database in your local machine

On your local machine, you must first connect to the mysql daemon

```sh
      mysql -u root -p
```

You need to know the password of your root user. Enter the password of your root user and you are finally connected as a super user to MySQL
Now, you just have to create a user and your database for the connection.

```sh 
      CREATE DATABASE sys_interoperability;

      CREATE USER 'restAPIUser'@'localhost' IDENTIFIED BY 'password';

      GRANT ALL PRIVILEGES ON sys_interoperability.* TO 'restAPIUser'@'localhost';

      FLUSH PRIVILEGES;

      QUIT
```

### Initialize the database in your docker container

You have the choice between two methods to create the database and the user who will have the rights to modify this database.

To do this, you must define in the [config/environment](./config/environment) folder, a file [sql-db.env](./config/environment/sql-db.env) which will be loaded at the start of the containerization of the MySQL image.
Then simply set the following environment variables:

```sh
      MYSQL_ROOT_PASSWORD=root-password        # Corresponds to the MySQL root user password
      MYSQL_DATABASE=sys_interoperability      # Corresponds to the MySQL database 
      MYSQL_USER=user                          # Corresponds to the MySQL user who has access to the declared database
      MYSQL_PASSWORD=user-password             # Corresponds to the MySQL user password
```

The second option is to do this manually.
With docker, your must first access to your docker container. In this project, the container name is 'sys-interoperability-database'.

```sh
      docker exec -it sys-interoperability-database /bin/bash
```

Once access to the docker container has been established, you have to connect to the mysql daemon :

```sh
      mysql -u root -p
```

You need to know the password of your root user. Enter the password of your root user and you are finally connected as a super user to MySQL
Now, you just have to create a user and your database for the connection.

```sh
      CREATE DATABASE sys_interoperability;

      CREATE USER 'restAPIUser'@'%' IDENTIFIED BY 'password';

      GRANT ALL PRIVILEGES ON sys_interoperability.* TO 'restAPIUser'@'%';

      FLUSH PRIVILEGES;

      QUIT
```

If you use a MySQL docker container on your local machine, you should create a sample iptables rule to open linux iptables firewall.

```sh
      iptables -A INPUT -i eth0 -p tcp --destination-port 3306 -j ACCEPT
```

In the Server, we only want to allow remote connection from the web API. 
For that, you should create a sample iptables rule which only allows access to the MySQL docker container to an ip address (that of the web API)

```sh
      iptables -A INPUT -i eth0 -s <ip_address> -p tcp --destination-port 3306 -j ACCEPT
```

For security reasons, Docker configures the iptables rules to prevent containers from forwarding traffic from outside the host machine, on Linux hosts. 
Docker sets the default policy of the FORWARD chain to DROP.

If the container running on host1 needs the ability to communicate directly with a container on host2, you need a route from host1 to host2.
Setting the policy to ACCEPT accomplishes this.

```sh
      sudo iptables -P FORWARD ACCEPT
```
