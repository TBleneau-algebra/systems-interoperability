package hr.algebra.tbleneau.restapi.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.algebra.tbleneau.restapi.constant.TokenConstant;
import hr.algebra.tbleneau.restapi.entity.RoleEntity;
import hr.algebra.tbleneau.restapi.entity.UserEntity;
import hr.algebra.tbleneau.restapi.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SecurityJWTokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    SecurityJWTokenAuthenticationFilter(UserRepository userRepository, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {

        try {
            UserEntity userEntity = new ObjectMapper().readValue(req.getInputStream(), UserEntity.class);
            UserEntity userExist = this.userRepository.findByUsername(userEntity.getUsername());
            List<GrantedAuthority> roles = new ArrayList<>();

            if (userExist != null) {
                Set<String> authorities = new HashSet<>();

                for (RoleEntity roleEntity : userExist.getRoles()) {
                    authorities.add(roleEntity.getName());
                }

                roles = AuthorityUtils.commaSeparatedStringToAuthorityList(String.join(",", authorities));
            }
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userEntity.getUsername(), userEntity.getPassword(), roles));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain chain, Authentication auth) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        UserEntity userEntity = userRepository.findByUsername(((User) auth.getPrincipal()).getUsername());
        ResponseEntity<UserEntity> responseEntity = new ResponseEntity<>(userEntity, HttpStatus.OK);

        Cookie JWTCookie = SecurityCookieService.create(TokenConstant.TOKEN_NAME, SecurityJWTokenService.createToken(userEntity), false, false, TokenConstant.TOKEN_EXPIRATION_TIME, "localhost");
        Cookie AuthCookie = SecurityCookieService.create(TokenConstant.TOKEN_AUTHENTICATED_NAME, String.valueOf(true), false, false, JWTCookie.getMaxAge(), httpServletRequest.getServerName());

        httpServletResponse.addCookie(JWTCookie);
        httpServletResponse.addCookie(AuthCookie);
        httpServletResponse.setContentType("application/json");
        httpServletResponse.getOutputStream().println(objectMapper.writeValueAsString(responseEntity));
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseEntity<UserEntity> responseBodyEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        response.setContentType("application/json");
        response.getOutputStream().println(objectMapper.writeValueAsString(responseBodyEntity));
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
}
