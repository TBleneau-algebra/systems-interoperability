package hr.algebra.tbleneau.restapi.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = ValidPasswordConstraint.class)
@Target({FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface ValidPassword {

    String message() default "Password is invalid. Password should not be less than 8 characters. " +
            "Password should not be greater than 64 characters. " +
            "Password should contain at least 1 special character, 1 uppercase, 1 lowercase.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
