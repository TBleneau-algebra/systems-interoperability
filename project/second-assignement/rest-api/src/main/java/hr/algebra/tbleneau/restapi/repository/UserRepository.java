package hr.algebra.tbleneau.restapi.repository;


import hr.algebra.tbleneau.restapi.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByUsername(String username);

    List<UserEntity> findAllByEmailLikeOrUsernameLikeOrNameLikeOrLastnameLike(String email, String username, String name, String lastname);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);
}
