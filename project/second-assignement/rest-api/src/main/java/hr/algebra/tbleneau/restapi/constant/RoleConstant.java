package hr.algebra.tbleneau.restapi.constant;

public enum RoleConstant {
    ROLE_USER,
    ROLE_ADMIN
}
