package hr.algebra.tbleneau.restapi.controller;

import hr.algebra.tbleneau.restapi.constant.RoleConstant;
import hr.algebra.tbleneau.restapi.entity.UserEntity;
import hr.algebra.tbleneau.restapi.repository.RoleRepository;
import hr.algebra.tbleneau.restapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    private final JmsTemplate jmsTemplate;

    @Autowired
    public UserController(UserRepository userRepository, RoleRepository roleRepository,
                          PasswordEncoder passwordEncoder, JmsTemplate jmsTemplate) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.jmsTemplate = jmsTemplate;
    }

    @RequestMapping(method = GET, path = "", produces = "application/json")
    public ResponseEntity<List<UserEntity>> get() {
        List<UserEntity> userEntities = this.userRepository.findAll();

        if (userEntities.size() == 1) {
            jmsTemplate.convertAndSend("Success - " + userEntities.size() + " user was found");
        } else {
            jmsTemplate.convertAndSend("Success - " + userEntities.size() + " users were found");
        }

        return ResponseEntity.status(HttpStatus.OK).body(userEntities);
    }

    @RequestMapping(method = GET, path = "/search", produces = "application/json")
    public ResponseEntity<List<UserEntity>> get(@RequestParam() String searchTerm) {
        List<UserEntity> userEntities = this.userRepository
                .findAllByEmailLikeOrUsernameLikeOrNameLikeOrLastnameLike(searchTerm, searchTerm, searchTerm, searchTerm);

        if (userEntities.size() == 1) {
            jmsTemplate.convertAndSend("Success - " + userEntities.size() + " user was found with the search term : " + searchTerm);
        } else {
            jmsTemplate.convertAndSend("Success - " + userEntities.size() + " users were found with the search term : " + searchTerm);
        }

        return ResponseEntity.status(HttpStatus.OK).body(userEntities);
    }

    @RequestMapping(method = POST, path = "", produces = "application/json")
    public ResponseEntity<UserEntity> post(@Valid @RequestBody UserEntity userEntity, Errors errors) {

        if (errors.hasErrors() || !this.roleRepository.existsByName(RoleConstant.ROLE_USER.name())) {
            jmsTemplate.convertAndSend("Error - Bad Request, the request body is not valid");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        if (this.userRepository.existsByEmail(userEntity.getEmail()) ||
                this.userRepository.existsByUsername(userEntity.getUsername())) {
            jmsTemplate.convertAndSend("Error - Conflict, email address or username already used");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }

        userEntity.getRoles().add(this.roleRepository.findByName(RoleConstant.ROLE_USER.name()));
        userEntity.setPassword(this.passwordEncoder.encode(userEntity.getPassword()));

        this.userRepository.save(userEntity);

        jmsTemplate.convertAndSend("Success - The user " + userEntity.getName() + " " + userEntity.getLastname() + " has been created");
        return ResponseEntity.status(HttpStatus.CREATED).body(userEntity);
    }

    @RequestMapping(method = PUT, path = "", produces = "application/json")
    public ResponseEntity<UserEntity> put(@RequestBody UserEntity userEntity, Errors errors) {
        if (errors.hasErrors()) {
            jmsTemplate.convertAndSend("Error - Bad Request, the request body is not valid");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        UserEntity userToUpdate = this.userRepository.findByUsername(userEntity.getUsername());

        if (this.userRepository.existsByUsername(userEntity.getUsername()) &&
                !userEntity.getUsername().equals(userToUpdate.getUsername())) {
            jmsTemplate.convertAndSend("Error - Conflict, username already used");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }

        if (this.userRepository.existsByEmail(userEntity.getEmail()) &&
                !userEntity.getEmail().equals(userToUpdate.getEmail())) {
            jmsTemplate.convertAndSend("Error - Conflict, email already used");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }

        userToUpdate.setEmail(userEntity.getEmail());
        userToUpdate.setUsername(userEntity.getUsername());
        userToUpdate.setLastname(userEntity.getLastname());
        userToUpdate.setName(userEntity.getName());

        this.userRepository.save(userToUpdate);

        jmsTemplate.convertAndSend("Success - The user " + userToUpdate.getName() + " " + userToUpdate.getLastname() + " has been updated");
        return ResponseEntity.status(HttpStatus.OK).body(userToUpdate);
    }

    @RequestMapping(value = "/{id}", method = DELETE, produces = "application/json")
    public ResponseEntity<UserEntity> delete(@PathVariable(value = "id") Long id) {
        if (!this.userRepository.existsById(id)) {
            jmsTemplate.convertAndSend("Error - Not found, user doesn't exist");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        UserEntity user = this.userRepository.findById(id).get();

        this.userRepository.delete(user);

        jmsTemplate.convertAndSend("Success - The user " + user.getName() + " " + user.getLastname() + " has been deleted");
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }
}
