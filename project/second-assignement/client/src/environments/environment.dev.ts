export const environment = {
  production: false,
  development: true,

  apiHost: 'http://localhost:8080/api'
};
