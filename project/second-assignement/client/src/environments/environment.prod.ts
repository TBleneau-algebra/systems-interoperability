export const environment = {
  production: true,
  development: false,

  apiHost: 'http://localhost:8080/api'
};
