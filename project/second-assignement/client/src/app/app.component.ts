import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {

  /**
   * @description Constructor of AppComponent component
   *
   * The constructor creates an instance of the AppComponent component and specifies the default values
   * input and output variables of the component.
   */
  constructor() {
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
  }
}
