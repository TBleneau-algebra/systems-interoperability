/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's modules
 */
import {LayoutModule} from './layout/layout.module';
import {HeaderModule} from './header/header.module';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    HeaderModule
  ],
  exports: [
    LayoutModule,
    HeaderModule
  ]
})

export class ComponentsModule {
}
