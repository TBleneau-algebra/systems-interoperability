import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-layout',
  styleUrls: ['./layout.component.scss'],
  template: `
    <nb-layout windowMode>
      <nb-layout-header fixed style="border-bottom: 1px solid #e4e9f2;">
        <ng-content select="app-header"></ng-content>
      </nb-layout-header>

      <nb-layout-column>
        <ng-content select="app-dashboard-layout"></ng-content>
      </nb-layout-column>
    </nb-layout>`,
})
export class LayoutComponent implements OnInit {

  /**
   * @description Constructor of the LayoutComponent component
   *
   * The constructor creates an instance of the LayoutComponent component and specifies the default values
   * of the component's input and output variables.
   */
  constructor() {
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
  }
}
