import {Component, Input, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert/alert.service';
import {AuthenticationService} from '../../services/web-service/authentication.service';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  /**
   * @description Constructor of HeaderComponent component
   *
   * The constructor creates an instance of the HeaderComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param alertService A reference to the alert service of the application
   * @param authenticationService A reference to the authentication service of the application
   * @param router Angular Router enables navigation from one view to the next as users perform application tasks
   * @param zone An injectable service for executing work inside or outside of the Angular zone
   */
  constructor(private alertService: AlertService,
              private authenticationService: AuthenticationService, private router: Router, private zone: NgZone) {
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {

  }

  /**
   * @description A method to log out the user via the RequestAuthenticationService service
   */
  logout(): void {
    this.alertService.confirm('Are you sure you want to log out?', 'Yes', 'No')
      .subscribe(response => {

        if (response) {
          this.authenticationService.get('/logout')
            .then(() => {
              this.authenticationService.user.next(null);
              this.navigate('/authentication');
            });
        }
      });
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   */
  navigate(path: string): void {
    this.router.navigate([path]).then();
  }

}
