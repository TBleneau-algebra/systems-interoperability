import {PipeTransform, Pipe} from '@angular/core';

@Pipe({
    name: 'productFilter',
    pure: false
})
export class ProductFilterPipe implements PipeTransform {

    /**
     * @description Constructor of HighlightPipe pipe
     *
     * The constructor creates an instance of the HighlightPipe pipe and specifies the default values
     * input and output variables of the pipe.
     */
    constructor() {
    }

    /**
     * @description The 'transform' method of the PipeTransform interface allows to perform a transformation.
     *
     * It is used to find an occurrence between a text (given as the first argument) and a list of products.
     * If an occurrence is found, the text is automatically highlighted.
     *
     * @param products Products list in which to search for occurrence for highlighting
     * @param searchKey Search term with which to perform the occurrence search for highlighting
     */
    transform(products: any[], searchKey: string) {
        if (!products) {
            return [];
        }
        if (!searchKey) {
            return products;
        }
        searchKey = searchKey.toLowerCase();
        return products.filter(product => product.label.toLowerCase().includes(searchKey));
    }
}
