/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's pipes
 */
import {SafeResourceUrlPipe} from './safe-resource-url.pipe';
import {HighlightPipe} from './highlight.pipe';
import {ReplacePipe} from './replace.pipe';
import {ProductFilterPipe} from './product-filter.pipe';
import {SafeResourceHtmlPipe} from './safe-resource-html.pipe';

@NgModule({
    imports: [
        CommonModule,
    ],
    exports: [
        CommonModule,
        SafeResourceUrlPipe,
        SafeResourceHtmlPipe,
        HighlightPipe,
        ReplacePipe,
        ProductFilterPipe
    ],
    declarations: [
        SafeResourceUrlPipe,
        SafeResourceHtmlPipe,
        HighlightPipe,
        ReplacePipe,
        ProductFilterPipe
    ]
})
export class PipesModule {
}
