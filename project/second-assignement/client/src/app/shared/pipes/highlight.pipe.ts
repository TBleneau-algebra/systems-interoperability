import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'highlight'
})
export class HighlightPipe implements PipeTransform {

    /**
     * @description Constructor of HighlightPipe pipe
     *
     * The constructor creates an instance of the HighlightPipe pipe and specifies the default values
     * input and output variables of the pipe.
     */
    constructor() {
    }

    /**
     * @description The 'transform' method of the PipeTransform interface allows to perform a transformation.
     *
     * It is used to find an occurrence between a text (given as the first argument) and a search element.
     * If an occurrence is found, the text is automatically highlighted.
     *
     * @param text Reference text in which to search for occurrence for highlighting
     * @param search Search term with which to perform the occurrence search for highlighting
     */
    transform(text: string, search: string): string {

        if (search) {
            let pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');

            pattern = pattern.split(' ').filter((t) => {
                return t.length > 0;
            }).join('|');

            let regex = new RegExp(pattern, 'gi');
            return search ? text.replace(regex, (match) => `<span class="highlight">${match}</span>`) : text;
        }
        return text;
    }
}
