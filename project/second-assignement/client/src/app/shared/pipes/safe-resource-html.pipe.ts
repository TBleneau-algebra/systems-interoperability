import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
    name: 'safeResourceHtml'
})
export class SafeResourceHtmlPipe implements PipeTransform {

    /**
     * @description Constructor of SafeResourceHtmlPipe pipe
     *
     * The constructor creates an instance of the SafeResourceHtmlPipe pipe and specifies the default values
     * input and output variables of the pipe.
     *
     * @param sanitizer A reference to the Angular service that helps prevent XSS security breaches
     * by disinfecting the values so that they can be used safely in the different contexts of the DOM.
     **/
    constructor(private sanitizer: DomSanitizer) {
    }

    /**
     * @description The 'transform' method of the PipeTransform interface allows to perform a transformation.
     *
     * It is used to bypass the security check regarding the authenticity of the source of an item (for example a source
     * of image returned by the API).
     *
     * @param value Value to be transformed
     * @param args Optional list of arguments
     */
    transform(value: any, args?: any): any {
        return this.sanitizer.bypassSecurityTrustHtml(value);
    }
}
