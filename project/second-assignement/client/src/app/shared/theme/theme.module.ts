/**
 * Import of Angular's modules
 */
import {LOCALE_ID, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of Nebular's modules
 */
import {
  NbAccordionModule,
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbContextMenuModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbPopoverModule,
  NbProgressBarModule,
  NbSearchModule,
  NbSelectModule,
  NbSidebarModule,
  NbStepperModule,
  NbTabsetModule,
  NbThemeModule,
  NbToastrModule,
  NbToggleModule,
  NbUserModule,
} from '@nebular/theme';
import {NbAuthModule} from '@nebular/auth';
import {NbSecurityModule} from '@nebular/security';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {NbDateFnsDateModule} from '@nebular/date-fns';
/**
 * Import of application's style
 */
import {corporateStyle} from './styles/corporate.style';

const NB_MODULES = [
  NbLayoutModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbContextMenuModule,
  NbSecurityModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbEvaIconsModule,
  NbCardModule,
  NbListModule,
  NbAccordionModule,
  NbToastrModule,
  NbDialogModule,
  NbStepperModule,
  NbToggleModule,
  NbAlertModule,
  NbTabsetModule,
  NbProgressBarModule,
  NbAuthModule,
  NbInputModule,
  NbDatepickerModule,
  NbCheckboxModule,
  NbDateFnsDateModule,
  NbPopoverModule
];


@NgModule({
  imports: [
    CommonModule,
    ...NB_MODULES
  ],
  exports: [
    ...NB_MODULES
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr-FR'},
  ],
  declarations: []
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: ThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          {
            name: 'corporate',
          },
          [corporateStyle],
        ).providers,
      ],
    };
  }
}
