import {NbJSThemeOptions} from '@nebular/theme';

/**
 * Export of the application theme
 */
export const corporateStyle: NbJSThemeOptions = {
    name: 'corporate',
    base: 'corporate',
    variables: {}
};
