import {Inject, Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DOCUMENT, Location} from '@angular/common';
import {tap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    /**
     * @description Constructor of HttpInterceptorService service
     *
     * The constructor creates an instance of the HttpInterceptorService service and specifies the default values
     * input and output variables of the service.
     *
     * @param document A DI Token representing the main rendering context. In a browser this is the DOM Document
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param location A service that applications can use to interact with a browser's URL
     */
    constructor(@Inject(DOCUMENT) private document, private router: Router, private location: Location) {
    }

    /**
     * @description This method intercepts an http request to add information such as headers, JavaScript cookies...
     *
     * @param request An outgoing HTTP request with an optional typed body.
     * @param next Transforms an HttpRequest into a stream of HttpEvents, one of which will likely be a HttpResponse.
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({withCredentials: true});
        request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
        request = request.clone({headers: request.headers.set('Origin-Request-Front', this.document.location.host)});

        return next.handle(request).pipe(tap(() => {
            },
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        this.router.navigate(['/authentication/login'], {
                            state: {
                                previousUrl: this.location.path()
                            }
                        }).then();
                    }
                }
            }
        ));
    }
}
