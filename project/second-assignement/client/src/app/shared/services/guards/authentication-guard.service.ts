import {AuthenticationService} from '../web-service/authentication.service';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate {

  /**
   * @description Constructor of AuthenticationGuardService service
   *
   * The constructor creates an instance of the AuthenticationGuardService service and specifies the default values
   * the input and output variables of the service.
   *
   * @param authenticationService A reference to the RequestAuthenticationService service
   * @param router Angular Router enables navigation from one view to the next as users perform application tasks
   */
  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  /**
   *  @description This method is a guard and decides if a route can be activated
   *  If all guards return true, navigation will continue.
   *  If any guard returns false, navigation will be cancelled.
   *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree
   *  returned from the guard.
   *
   * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
   * @param state represents the state of the router at a moment in time.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
    boolean | UrlTree {
    return new Promise(resolve => {
      this.authenticationService.get('/user/me')
        .then(response => {
          if (!response.responseData.roles.find(item => item.name === 'ROLE_ADMIN')) {
            (state.url.slice(0, 15) === '/authentication') ? resolve(true) : resolve(this.router.parseUrl('/authentication'));
          }

          this.authenticationService.user.next(response.responseData);
          (state.url.slice(0, 15) === '/authentication') ? resolve(this.router.parseUrl('/')) : resolve(true);
        })
        .catch(() => {
          (state.url.slice(0, 15) === '/authentication') ? resolve(true) : resolve(this.router.parseUrl('/authentication'));
        });
    });
  }
}
