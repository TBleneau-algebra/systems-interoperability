import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {RequestClass} from './request.class';

@Injectable({
  providedIn: 'root'
})
export class RequestService extends RequestClass {

  /**
   * @description Constructor of RequestService service
   *
   * The constructor creates an instance of the RequestService service and specifies the default values
   * input and output variables of the service.
   *
   * @param httpClient A reference to Angular's internal httpClient service
   * @param location A reference to the Location service that allows to build a URL from the root
   */
  constructor(httpClient: HttpClient, location: Location) {
    super(httpClient, location, environment.apiHost);
  }
}
