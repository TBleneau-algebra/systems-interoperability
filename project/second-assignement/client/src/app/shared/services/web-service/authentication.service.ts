import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {User} from '../../model/user/user.class';
import {CustomSubjectClass} from '../../model/rxjs/custom-subject.class';
import {RequestClass} from './request.class';
import {environment} from '../../../../environments/environment';

@Injectable({providedIn: 'root'})
export class AuthenticationService extends RequestClass {

  /**
   * @description Current user connected to the application
   */
  private _user: CustomSubjectClass<User>;

  /**
   * @description Constructor of AuthenticationService service
   *
   * The constructor creates an instance of the AuthenticationService service and specifies the default values
   * input and output variables of the service.
   *
   * @param httpClient A reference to Angular's internal httpClient service
   * @param location A reference to the Location service that allows to build a URL from the root
   */
  constructor(httpClient: HttpClient, location: Location) {
    super(httpClient, location, environment.apiHost);

    this.user = new CustomSubjectClass<User>();
  }

  /**
   * @description The method allows you to retrieve the current user connected to the application
   */
  get user(): CustomSubjectClass<User> {
    return this._user;
  }

  /**
   * @description The method allows to define the current user connected to the application
   *
   * @param value Value of the current user connected to the application
   */
  set user(value: CustomSubjectClass<User>) {
    this._user = value;
  }
}
