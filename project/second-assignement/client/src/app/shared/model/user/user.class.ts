/**
 * @description Mapping class
 */
export class User {
  id: number;
  name: string;
  lastname: string;
  username: string;
  email: string;
  password: string;
  roles: Array<any>;

  constructor() {
    this.roles = new Array<any>();
  }
}
