import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class ValidatorClass {

    static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!control.value) {
                return null;
            }
            const valid = regex.test(control.value);
            return valid ? null : error;
        };
    }

    static passwordMatchValidator(control: AbstractControl) {
        const password: string = control.get('password').value;
        const confirmPassword: string = control.get('confirmPassword').value;

        if (confirmPassword === '' || confirmPassword === null) {
            control.get('confirmPassword').setErrors({required: true});
        } else if (password !== confirmPassword) {
            control.get('confirmPassword').setErrors({passswordMatch: true});
        }
    }
}
