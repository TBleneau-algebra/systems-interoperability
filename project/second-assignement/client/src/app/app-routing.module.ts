/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
/**
 * Import of application's components
 */
import {DashboardComponent} from './layouts/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'authentication',
    loadChildren: () => import('./layouts/authentication/authentication.module')
      .then(m => m.AuthenticationModule),
  },
  {
    path: '',
    component: DashboardComponent
  },
  {path: '**', redirectTo: '', pathMatch: 'full'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false,
      paramsInheritanceStrategy: 'always',
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
