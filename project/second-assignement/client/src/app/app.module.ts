/**
 * Import of Angular's modules
 */
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LOCALE_ID, NgModule} from '@angular/core';
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
/**
 * Import of application's modules
 */
import {ThemeModule} from './shared/theme/theme.module';
import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './shared/components/components.module';
import {LayoutsModule} from './layouts/layouts.module';
/**
 * Import of application's service
 */
import {HttpInterceptorService} from './shared/services/http-interceptor/http-interceptor.service';
/**
 * Import of application's component
 */
import {AppComponent} from './app.component';
import {NbSecurityModule} from '@nebular/security';
import {NbAuthModule} from '@nebular/auth';
import {NbDatepickerModule, NbDialogModule, NbMenuModule, NbSidebarModule, NbToastrModule, NbWindowModule} from '@nebular/theme';

/**
 * @description Register global data to be used internally by Angular.
 */
registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    ThemeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbAuthModule.forRoot(),
    NbSecurityModule.forRoot(),
    AppRoutingModule,
    LayoutsModule,
    ComponentsModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true}
  ],
})
export class AppModule {
}
