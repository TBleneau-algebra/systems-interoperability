import {Component} from '@angular/core';

@Component({
  selector: 'app-dashboard',
  template: `
    <app-layout>
      <app-header></app-header>
      <app-dashboard-layout></app-dashboard-layout>
    </app-layout>`
})
export class DashboardComponent {
}
