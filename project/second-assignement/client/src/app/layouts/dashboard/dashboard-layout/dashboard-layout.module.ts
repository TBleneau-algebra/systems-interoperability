/**
 * Import of Angular's module
 */
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../../shared/theme/theme.module';
/**
 * Import of application's components
 */
import {DashboardLayoutComponent} from './dashboard-layout.component';
/**
 * Import of ng2-smart-table modules
 */
import {Ng2SmartTableModule} from 'ng2-smart-table';

@NgModule({
  imports: [
    ThemeModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    RouterModule,
    CommonModule,
    Ng2SmartTableModule
  ],
  declarations: [
    DashboardLayoutComponent,
  ],
  exports: [
    DashboardLayoutComponent
  ]
})
export class DashboardLayoutModule {
}
