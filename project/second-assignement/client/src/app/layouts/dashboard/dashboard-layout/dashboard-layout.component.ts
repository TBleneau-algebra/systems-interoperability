import {Component, OnInit} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Router} from '@angular/router';
import {RequestService} from '../../../shared/services/web-service/request.service';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {User} from '../../../shared/model/user/user.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidatorClass} from '../../../shared/model/validator/validator.class';
import {RequestParamsClass} from '../../../shared/services/web-service/params/request-params.class';
import {RequestParamsInterface} from '../../../shared/services/web-service/params/request-params.interface';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html'
})
export class DashboardLayoutComponent implements OnInit {

  /**
   * @description User model used to create a new user
   */
  private _user: User;

  /**
   * @description Data retrieved via the API
   */
  private _data: any;

  /**
   * @description Tracks the value and validity state of a group of FormControl instances
   */
  private _formGroup: FormGroup;

  /**
   * @description Configuration parameter of the Ng2SmartTable component
   */
  private _settings: any;

  /**
   * @description Class (object) to be provided to the Ng2SmartTable component to load local data
   */
  private _source: LocalDataSource;

  /**
   * @description Constructor of AdministrationComponent component
   *
   * The constructor creates an instance of the AdministrationComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param router Angular Router enables navigation from one view to the next as users perform application tasks
   * @param requestService A reference to the http request service of the application
   * @param alertService A reference to the alert service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   */
  constructor(private router: Router, private requestService: RequestService, private alertService: AlertService,
              private formBuilder: FormBuilder) {
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.user = new User();
    this.settings = {
      actions: {
        add: false,
        edit: true,
        delete: true
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        id: {
          title: 'ID',
          type: 'number',
          filter: false,
          editable: false
        },
        name: {
          title: 'First Name',
          type: 'string',
          filter: false,
          editable: true
        },
        lastname: {
          title: 'Last Name',
          type: 'string',
          filter: false,
          editable: true
        },
        username: {
          title: 'Username',
          type: 'string',
          filter: false,
          editable: true
        },
        email: {
          title: 'Email',
          type: 'string',
          filter: false,
          editable: true
        },
        rolesJoined: {
          title: 'Roles',
          type: 'string',
          filter: false,
          editable: false
        },
      },
    };

    this.formsInit();
    this.initData();
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to create a user in the application
   *
   * It calls the method Post of the RequestAuthenticationService service
   */
  create(): void {
    this.requestService.post('/user/register', this.user)
      .then(() => {
        delete this.user;

        this.user = new User();
        this.alertService.success('User has been created successfully!');

        this.initData();
      })
      .catch((error) => {
        this.errorEvent(error.responseError);
      });
  }

  /**
   * @description This method checks if a HTML element is focused
   *
   * @param value HTML element to check
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method allows to confirm changes before they are applied to the table data source.
   *
   * @param event User click event on delete button
   */
  delete(event): void {
    this.alertService.confirm('Are you sure you want to delete this user?', 'Yes', 'No')
      .subscribe(value => {
        if (value) {

          const params: RequestParamsInterface = new RequestParamsClass();

          params.params = [{name: 'id', value: event.data.id}];

          this.requestService.delete('/user/delete', null, params)
            .then(() => {
              this.alertService.success('User has been correctly deleted');
              event.confirm.resolve();
            })
            .catch((error) => {
              if (error.responseStatus === 403) {
                this.alertService.error('You can\'t delete an administrator user');
              } else {
                this.errorEvent(error.responseError);
              }
              event.confirm.reject();
            });
        }
      });
  }

  /**
   * @description This method allows to edit user's information
   *
   * @param event User click event
   */
  edit(event: any): void {
    if (event.data.id === event.newData.id && JSON.stringify(event.data) !== JSON.stringify(event.newData)) {
      this.requestService.put('/user/update', event.newData)
        .then(() => {
          this.alertService.success('User has been correctly updated');
          event.confirm.resolve();
        })
        .catch((error) => {
          if (error.responseStatus === 409) {
            this.alertService.error('Email address or username already exists!');
          } else {
            this.errorEvent(error.responseError);
          }

          this.source.update(event.data, event.data);
          event.confirm.resolve();
        });
    } else {
      this.source.update(event.data, event.data);
      event.confirm.resolve();
    }
  }

  /**
   * @description This method allows you to search in the data table
   *
   * @param query search parameter to be applied as a filter
   */
  search(query: string = ''): void {
    if (query) {
      const params: RequestParamsInterface = new RequestParamsClass();

      params.params = [{name: 'searchTerm', value: '%' + query + '%'}];

      this.requestService.get('/user/search', null, params)
        .then((response) => this.successEvent(response.responseData))
        .catch((error) => {
          this.errorEvent(error.responseError);
        });
    } else {
      this.initData();
    }
  }

  /**
   * @description The method is used to initialize the state of the values of a group of FormControl instances
   */
  private formsInit(): void {
    const regExp = new RegExp('^((?:\\w|[\\-_ ](?![\\-_ ])|[\\u00C0\\u00C1\\u00C2\\u00C3\\u00C4\\u00C5\\u00C6\\u00C7\\u00C8\\' +
      'u00C9\\u00CA\\u00CB\\u00CC\\u00CD\\u00CE\\u00CF\\u00D0\\u00D1\\u00D2\\u00D3\\u00D4\\u00D5\\u00D6\\u00D8\\u00D9\\u00DA\\u00DB' +
      '\\u00DC\\u00DD\\u00DF\\u00E0\\u00E1\\u00E2\\u00E3\\u00E4\\u00E5\\u00E6\\u00E7\\u00E8\\u00E9\\u00EA\\u00EB\\u00EC\\u00ED\\u00' +
      'EE\\u00EF\\u00F0\\u00F1\\u00F2\\u00F3\\u00F4\\u00F5\\u00F6\\u00F9\\u00FA\\u00FB\\u00FC\\u00FD\\u00FF\\u0153])+)$', 'i');

    this.formGroup = this.formBuilder.group(
      {
        name: [null, Validators.compose([
          Validators.required,
          Validators.pattern(regExp)
        ])],
        lastname: [null, Validators.compose([
          Validators.required,
          Validators.pattern(regExp)
        ])],
        email: [null, Validators.compose([
          Validators.required,
          Validators.email
        ])],
        username: [null, Validators.compose([
          Validators.required
        ])],
        password: [null, Validators.compose([
          Validators.required,
          ValidatorClass.patternValidator(/\d/, {hasNumber: true}),
          ValidatorClass.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
          ValidatorClass.patternValidator(/[a-z]/, {hasSmallCase: true}),
          ValidatorClass.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/, {hasSpecialCharacters: true}),
          Validators.minLength(8),
          Validators.maxLength(64)
        ])]
      }
    );
  }

  /**
   * @description This method is called in the component initialization cycle to retrieve data via the API
   */
  private initData(): void {
    this.requestService.get('/user')
      .then((response) => this.successEvent(response.responseData))
      .catch((error) => {
        this.errorEvent(error.responseError);
      });
  }

  /**
   * @description A method method that sends the error message received by the API
   *
   * @param data Data with errors messages sent by the API
   */
  private errorEvent(data: any) {
    this.alertService.error('An error has occurred. Problem connecting to the API');
  }

  /**
   * @description A method method that sends a success event
   *
   * @param data Response sent by the API
   */
  private successEvent(data: Array<any>): void {
    data.forEach(item => {
      item['rolesJoined'] = (item['roles'] as Array<any>).map(elem => elem.name).join(' - ');
    });

    this._source = new LocalDataSource();
    this._source.load(data).then(() => {
      this.data = data;
    });

    (document.getElementsByClassName('ng2-smart-filters').length > 0) ?
      document.getElementsByClassName('ng2-smart-filters')[0].remove() : undefined;
  }

  /**
   * @description The method allows you to retrieve the data gotten via the API
   */
  get data(): any {
    return this._data;
  }

  /**
   * @description The method allows you to assign the data gotten via the API
   *
   * @param value Value of the data gotten via the API
   */
  set data(value: any) {
    this._data = value;
  }

  /**
   * @description The method allows you to retrieve the configuration parameter of the Ng2SmartTable component
   */
  get settings(): any {
    return this._settings;
  }

  /**
   * @description The method allows you to assign the configuration parameter of the Ng2SmartTable component
   *
   * @param value Value of the configuration parameter of the Ng2SmartTable component
   */
  set settings(value: any) {
    this._settings = value;
  }

  /**
   * @description The method allows you to retrieve the class (object) to be provided to the Ng2SmartTable component to load local data
   */
  get source(): LocalDataSource {
    return this._source;
  }

  /**
   * @description The method allows you to assign the class (object) to be provided to the Ng2SmartTable component to load local data
   *
   * @param value Value of the class (object) to be provided to the Ng2SmartTable component
   */
  set source(value: LocalDataSource) {
    this._source = value;
  }

  /**
   * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the variable which tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the user model used to create a new user
   */
  get user(): User {
    return this._user;
  }

  /**
   * @description The method allows you to assign the user model used to create a new user
   *
   * @param value Value of the user model used to create a new user
   */
  set user(value: User) {
    this._user = value;
  }
}
