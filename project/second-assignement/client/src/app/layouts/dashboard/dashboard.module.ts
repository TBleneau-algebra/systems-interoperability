/**
 * Import of Angular's module
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../shared/theme/theme.module';
import {DashboardLayoutModule} from './dashboard-layout/dashboard-layout.module';
import {ComponentsModule} from '../../shared/components/components.module';
/**
 * Import of application's components
 */
import {DashboardComponent} from './dashboard.component';

@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    ComponentsModule,
    DashboardLayoutModule
  ],
  declarations: [
    DashboardComponent,
  ],
  exports: [
    DashboardComponent
  ]
})
export class DashboardModule {
}
