/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's modules
 */
import {DashboardModule} from './dashboard/dashboard.module';
import {AuthenticationModule} from './authentication/authentication.module';


@NgModule({
  imports: [
    CommonModule,
    DashboardModule,
    AuthenticationModule
  ],
  exports: [
    DashboardModule,
    AuthenticationModule
  ]
})
export class LayoutsModule {
}
