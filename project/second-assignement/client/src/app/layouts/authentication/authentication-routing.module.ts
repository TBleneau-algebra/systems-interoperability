/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
/**
 * Import of application's components
 */
import {LoginComponent} from './login/login.component';
import {AuthenticationComponent} from './authentication.component';


const routes: Routes = [
  {
    path: '',
    component: AuthenticationComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: '**',
        redirectTo: '',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ]
})
export class AuthenticationRoutingModule {
}
