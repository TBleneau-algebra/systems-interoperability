import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../shared/services/web-service/authentication.service';
import {Subscription} from 'rxjs';
import {User} from '../../../shared/model/user/user.class';
import {AlertService} from '../../../shared/services/alert/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  /**
   * @description Previous url to navigate after login
   */
  private readonly previousUrl: string;

  /**
   * @description Credential model used to save information
   */
  private _credential: User;

  /**
   * @description Tracks the value and validity state of a group of FormControl instances
   */
  private _formGroup: FormGroup;

  /**
   * @description Offline status
   */
  private offline: boolean;

  /**
   * @description Array of Subscription objects that have an unsubscribe() method, which you call to stop
   * receiving event
   */
  private subscriptions: Array<Subscription>;

  /**
   * @description Constructor of LoginComponent component
   *
   * The constructor creates an instance of the LoginComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param loginService A reference to the authentication service
   * @param router Angular Router enables navigation from one view to the next as users perform application tasks
   * @param alertService A reference to the alert service of the application
   */
  constructor(private formBuilder: FormBuilder, private loginService: AuthenticationService, private router: Router,
              private alertService: AlertService) {
    this.previousUrl = (this.router.getCurrentNavigation().extras.state) ?
      this.router.getCurrentNavigation().extras.state.previousUrl : null;
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.subscriptions = [];
    this.credential = new User();

    this.formGroup = this.formBuilder.group(
      {
        username: [null, Validators.compose([
          Validators.required,
        ])],
        password: [null, Validators.compose([])]
      }
    );
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys to initialize the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * @description A method to connect the user via the AuthenticationService service
   */
  login(): void {
  }

  /**
   * A method which check if a HTMLElement is focused
   * @return void
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description A method method that sends the error message received by the API
   *
   * @param data Data with errors messages sent by the API
   */
  private errorEvent(data: any) {
    this.alertService.error('Incorrect username or password');
  }

  /**
   * @description A method method that sends a success event
   */
  private successEvent(): void {
    const path: string = (this.previousUrl) ? this.previousUrl : '/';
    this.subscriptions.forEach(subscription => subscription.unsubscribe());

    this.router.navigate([path]).then(() => {
      this.alertService.success('Login Successful!');
    });
  }

  /**
   * @description The method allows you to retrieve the credential model in the LoginComponent
   */
  get credential(): User {
    return this._credential;
  }

  /**
   * @description The method allows you to assign the credential model in the LoginComponent
   *
   * @param value Value of the credential model in the LoginComponent
   */
  set credential(value: User) {
    this._credential = value;
  }

  /**
   * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the variable which tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }
}
