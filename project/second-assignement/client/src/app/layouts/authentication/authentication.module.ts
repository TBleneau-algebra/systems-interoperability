/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
/**
 * Import of application's modules
 */
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {LoginModule} from './login/login.module';
import {ThemeModule} from '../../shared/theme/theme.module';
/**
 * Import of application's components
 */
import {AuthenticationComponent} from './authentication.component';

@NgModule({
  imports: [
    ThemeModule,
    AuthenticationRoutingModule,
    LoginModule
  ],
  declarations: [
    AuthenticationComponent
  ]
})
export class AuthenticationModule {
}
