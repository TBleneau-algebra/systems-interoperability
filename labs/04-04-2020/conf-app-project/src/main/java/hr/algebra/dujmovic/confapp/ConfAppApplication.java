package hr.algebra.dujmovic.confapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfAppApplication.class, args);
	}
}
