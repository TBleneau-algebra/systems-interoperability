/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.algebra.dujmovic.confapp.rest;

import hr.algebra.dujmovic.confapp.data.LectureRepository;
import hr.algebra.dujmovic.confapp.model.Lecture;

import java.util.List;
import java.util.Optional;

import hr.algebra.dujmovic.confapp.model.ResponseBodyEntity;
import hr.algebra.dujmovic.confapp.model.Speaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 *
 * @author matij
 */
@RestController
@RequestMapping(path = "/api/lecture", produces = "application/json")
@CrossOrigin(origins = "*")
public class LectureRestController {

    @Autowired
    LectureRepository repository;

    @GetMapping
    public Iterable<Lecture> findAll() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Lecture> findOne(@PathVariable Long id) {
        Optional<Lecture> lecture = repository.findById(id);
        
        if(lecture.isPresent()){
            return new ResponseEntity<>(lecture.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = "application/json")
    public Lecture save(@RequestBody Lecture lecture) {
        return repository.save(lecture);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Lecture> update(@PathVariable Long id, @RequestBody Lecture lecture) {
        if (repository.findById(id).isPresent()) {
            lecture.setId(id);
            lecture = repository.save(lecture);
            return new ResponseEntity<>(lecture, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/position")
    public ResponseEntity<ResponseBodyEntity<List<Lecture>>> get(@RequestParam() String position) {
        if (!isPositionExists(position)) {
            ResponseBodyEntity<List<Lecture>> responseBodyEntity = new ResponseBodyEntity<>(HttpStatus.BAD_REQUEST);

            responseBodyEntity.setMessage("BAD SPEAKER POSITION");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBodyEntity);
        }

        ResponseBodyEntity<List<Lecture>> responseBodyEntity = new ResponseBodyEntity<>(HttpStatus.OK);

        responseBodyEntity.setData(this.repository.findAllBySpeakerPosition(Speaker.Position.valueOf(position)));
        return ResponseEntity.status(HttpStatus.OK).body(responseBodyEntity);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }


    private boolean isPositionExists(String position) {
        for (Speaker.Position speakerPos : Speaker.Position.values()) {
            if (speakerPos.name().equals(position)) {
                return true;
            }
        }
        return false;
    }
}
