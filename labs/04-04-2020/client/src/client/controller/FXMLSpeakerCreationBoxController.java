package client.controller;

import client.services.HttpRequestService;
import client.services.HttpResponseBody;
import client.validator.TextFieldValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.algebra.dujmovic.confapp.model.Speaker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class FXMLSpeakerCreationBoxController implements Initializable {

    private Speaker speaker;

    private HttpRequestService httpRequestService;

    @FXML
    private TextField speakerName;

    @FXML
    private ComboBox<String> speakerPosition;

    @FXML
    private Button createButton;

    @FXML
    private Text errorName;

    @FXML
    private Text errorPosition;

    public FXMLSpeakerCreationBoxController() {
        this.speaker = new Speaker();
        this.httpRequestService = new HttpRequestService();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.disabled();

        this.initializeName();
        this.initializePosition();
    }

    @FXML
    private void create() {
        try {
            HttpResponseBody httpResponseBody = this.httpRequestService.post("http://localhost:8080/api/speaker/",
                    this.speaker, Collections.emptyMap(), Collections.emptyMap());

            if (httpResponseBody.getCode() == 201) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

                alert.setContentText("The speaker has been successfully created \n" + httpResponseBody.getData().toString());
                alert.showAndWait();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setContentText("An error has occurred during the creation of the speaker");
                alert.showAndWait();
            }
        } catch (IOException exception) {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setContentText("An error has occurred during the creation of the speaker");
            alert.showAndWait();
        }
        this.speaker = new Speaker();
        this.speakerName.setText(null);
    }

    private void initializePosition() {
        ObservableList<String> positions = FXCollections.observableArrayList("MID", "JUNIOR", "SENIOR");

        this.speakerPosition.setItems(positions);

        this.speakerPosition.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (TextFieldValidator.requiredValidation(newValue)) {
                errorPosition.setText("The position of the speaker is required");
            } else {
                errorPosition.setText(null);
                this.disabled();
            }
        });
    }

    private void initializeName() {
        this.speakerName.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue && !newValue && TextFieldValidator.requiredValidation(this.speakerName.getText())) {
                errorName.setText("The name of the speaker is required");
            } else {
                errorName.setText(null);
            }
        });

        this.speakerName.textProperty().addListener((observable, oldValue, newValue) -> {
            this.disabled();
        });
    }

    private void disabled() {
        if (TextFieldValidator.requiredValidation(this.speakerName.getText()) ||
                TextFieldValidator.requiredValidation(this.speakerPosition.getValue())) {
            this.createButton.setDisable(true);
        } else {
            this.createButton.setDisable(false);

            this.speaker.setPosition(Speaker.Position.valueOf(this.speakerPosition.getValue()));
            this.speaker.setName(this.speakerName.getText());
        }
    }
}
