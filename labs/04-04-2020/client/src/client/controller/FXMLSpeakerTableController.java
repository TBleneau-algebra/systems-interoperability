package client.controller;

import client.services.HttpRequestService;
import client.services.HttpResponseBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.algebra.dujmovic.confapp.model.Speaker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;

public class FXMLSpeakerTableController implements Initializable {

    private Speaker[] speakers;

    private HttpRequestService httpRequestService;

    private ObjectMapper objectMapper;

    @FXML
    private TableView<Speaker> speakerTableView;

    @FXML
    private TableColumn<Speaker, Integer> id;

    @FXML
    private TableColumn<Speaker, String> name;

    @FXML
    private TableColumn<Speaker, String> position;

    @FXML
    private TableColumn<Speaker, LocalDate> creationDate;

    @FXML
    private Button deleteButton;

    @FXML
    private TextField speakerID;

    @FXML
    private TextField fieldID;

    @FXML
    private TextField fieldName;

    @FXML
    private TextField fieldPosition;

    @FXML
    private TextField fieldCreationDate;

    public FXMLSpeakerTableController() {
        this.httpRequestService = new HttpRequestService();
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.initialize();
        this.getAll();
    }

    @FXML
    private void deleteByID() {
        HttpResponseBody httpResponseBody = this.httpRequestService.delete("http://localhost:8080/api/speaker/" + this.speakerID.getText(),
                Collections.emptyMap(), Collections.emptyMap());

        if (httpResponseBody.getCode() == 204) {
            this.getAll();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setContentText("The selected speaker does not exist.\nPlease select an existing speaker");
            alert.showAndWait();
        }

        this.speakerID.clear();
    }

    private void getAll() {
        try {
            HttpResponseBody httpResponseBody = this.httpRequestService.get("http://localhost:8080/api/speaker",
                    Collections.emptyMap(), Collections.emptyMap());

            if (httpResponseBody.getCode() == 200) {
                Speaker[] speakers = objectMapper.readValue(httpResponseBody.getData().toString(), Speaker[].class);

                this.speakers = speakers;
                if (speakers != null) {
                    List<Speaker> speakerList = Arrays.asList(speakers);
                    ObservableList<Speaker> observableList = FXCollections.observableArrayList(speakerList);

                    speakerTableView.getItems().setAll(observableList);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initialize() {
        this.disabled();

        this.id.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.name.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.position.setCellValueFactory(new PropertyValueFactory<>("position"));
        this.creationDate.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

        this.speakerID.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.isEmpty()) {
                Optional<Speaker> speaker = Arrays.stream(this.speakers).filter(x -> Long.parseLong(newValue) == x.getId()).findFirst();

                speaker.ifPresent(value -> this.fieldID.setText(value.getId().toString()));
                speaker.ifPresent(value -> this.fieldName.setText(value.getName()));
                speaker.ifPresent(value -> this.fieldPosition.setText(value.getPosition().name()));
                speaker.ifPresent(value -> this.fieldCreationDate.setText(value.getCreatedAt().toString()));
            } else {
                this.fieldID.setText(null);
                this.fieldName.setText(null);
                this.fieldPosition.setText(null);
                this.fieldCreationDate.setText(null);
            }
            this.disabled();
        });
    }

    private void disabled() {
        if (this.speakerID.getText() == null || this.speakerID.getText().isEmpty()) {
            this.deleteButton.setDisable(true);
        } else {
            this.deleteButton.setDisable(false);
        }
    }
}
